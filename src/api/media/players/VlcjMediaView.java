package api.media.players;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

public class VlcjMediaView extends AnchorPane{
	
	VlcjPlayer player;
	
	private Integer max_volume = 200;
	private Integer pref_volume = 100;
	private Integer jump_volume = 5;
	private Double playback_jump = 0.01;
	private Integer temp_volume = 0;
	
	public VlcjMediaView() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("VlcjMediaView.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {fxmlLoader.load();} catch (Exception exception) {throw new RuntimeException(exception);}
	}

	public Integer getMaxVolume() {
		return max_volume;
	}
	public void setMaxVolume(Integer max_volume) {
		this.max_volume = max_volume;
	}
	public Integer getPrefVolume() {
		return pref_volume;
	}
	public void setPrefVolume(Integer default_volume) {
		this.pref_volume = default_volume;
	}
	public Integer getJumpVolume() {
		return jump_volume;
	}
	public void setJumpVolume(Integer jump_volume) {
		this.jump_volume = jump_volume;
	}
	public Double getPlaybackJump() {
		return playback_jump;
	}
	public void setPlaybackJump(Double playback_jump) {
		this.playback_jump = playback_jump;
	}

	public void load(String file_path){
		getChildren().removeAll(getChildren());
		player = new VlcjPlayer();
		player.setMaxVolume(max_volume);
		player.setPrefVolume(pref_volume);
		player.setJumpVolume(jump_volume);
		player.setPlayBackJump(playback_jump);
		AnchorPane.setTopAnchor(player, 0.00);
		AnchorPane.setBottomAnchor(player, 0.00);
		AnchorPane.setLeftAnchor(player, 0.00);
		AnchorPane.setRightAnchor(player, 0.00);
		getChildren().add(player);
		setOnKeyReleased(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if(event.getCode() == KeyCode.RIGHT) {
					forward();
				}else if (event.getCode() == KeyCode.LEFT) {
					reverse();
				}else if (event.getCode() == KeyCode.UP) {
					volumeUp();
				}else if (event.getCode() == KeyCode.DOWN) {
					volumeDown();
				}else if (event.getCode() == KeyCode.SPACE) {
					if(paused()) {
						play();
					}else {
						pause();
					}
				}else if (event.getCode() == KeyCode.M) {
					if(muted()) {
						volumeUnMute();
					}else {
						volumeMute();
					}
				}
			}
		});
		player.load(file_path);
		requestFocus();
	}
	
	public void play(Double at_position) {
		player.play(at_position);
	}
	
	public void play(int at_position) {
		player.play(at_position);
	}
	
	public void play() {
		player.play();
	}
	
	public void stop() {
		player.stop();
	}
	
	public void pause(Double at_position) {
		player.pause(at_position);
	}
	
	public void pause(int at_position) {
		player.pause(at_position);
	}
	
	public void pause() {
		player.pause();
	}
	
	public void forward(Double value) {
		player.forward(value);
	}
	
	public void forward(int value) {
		player.forward(value);
	}
	
	public void forward() {
		player.forward();
	}
	
	public void reverse(Double value) {
		player.reverse(value);
	}
	
	public void reverse(int value) {
		player.reverse(value);
	}
	
	public void reverse() {
		player.reverse();
	}
	
	public boolean playing() {
		return player.isPlaying();
	}
	
	public boolean paused() {
		return player.isPaused();
	}
	
	public void volume(int value) {
		player.adjustVolume(value);
	}
	
	public int volume() {
		return player.getVolume();
	}
	
	public void volumeUp(int value) {
		player.volumeUp(value);
	}
	
	public void volumeUp() {
		player.volumeUp();
	}
	
	public void volumeDown(int value) {
		player.volumeDown(value);
	}
	
	public void volumeDown() {
		player.volumeDown();
	}
	
	public void volumeMute() {
		temp_volume = volume();
		player.volumeMute();
	}
	
	public void volumeUnMute() {
		volume(temp_volume);
		temp_volume = 0;
	}
	
	public boolean muted() {
		return !(volume() > 0);
	}

}