package api.media.players;

import java.nio.ByteBuffer;

import com.sun.jna.Memory;

import javafx.application.Platform;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;

public class VlcjController {

	private ImageView imageView;
    private DirectMediaPlayerComponent mediaPlayerComponent = null;
    private WritableImage writableImage;
    private Pane playerHolder;
    private WritablePixelFormat<ByteBuffer> pixelFormat;
    private FloatProperty videoSourceRatioProperty;
    Slider timeline;
	
    public VlcjController(Slider timeline) {
    	this.timeline = timeline;
    	if(mediaPlayerComponent != null) {mediaPlayerComponent.release();}    	
    	mediaPlayerComponent = new CanvasPlayerComponent();
        playerHolder = new Pane();
		videoSourceRatioProperty = new SimpleFloatProperty(0.4f);
		pixelFormat = PixelFormat.getByteBgraPreInstance();
		initializeImageView();
	}
    
    public static void gc() {
    	System.gc();
    }
    
	public static void addTimelineListener(Slider timeline, VlcjController controller) {
		controller.getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			
			boolean update_position = true;
			int update_position_interval = 0;
			
			@Override
			public void playing(MediaPlayer mediaPlayer) {
				super.playing(mediaPlayer);
				
				Runnable runnable_timeline_inputs = new Runnable() {
					
					@Override
					public void run() {
						
						timeline.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
							update_position = false;
						});

						timeline.addEventFilter(MouseEvent.MOUSE_DRAGGED, event -> {
							update_position = false;
							controller.getMediaPlayer().setPosition(((Double) timeline.getValue()).floatValue());
						});

						timeline.addEventFilter(MouseEvent.MOUSE_RELEASED, event -> {
							controller.getMediaPlayer().setPosition(((Double) timeline.getValue()).floatValue());
							update_position = true;
						});
					}
				};
				Thread thread_timeline_inputs = new Thread(runnable_timeline_inputs);
				thread_timeline_inputs.start();
				
				Runnable runnable_timeline_updates = new Runnable() {
					@Override
					public void run() {
						while (controller.getMediaPlayer().isPlaying()) {
							if(update_position == true) {
								if(update_position_interval > 9) {
									timeline.setValue(controller.getMediaPlayer().getPosition());
									update_position_interval = 0;
								}else {
									update_position_interval = update_position_interval + 1;
								}
							}
						}			
					}
				};
				Thread thread_timeline_updates = new Thread(runnable_timeline_updates);
				thread_timeline_updates.start();
				System.gc();	
			}
			
			@Override
			public void finished(MediaPlayer mediaPlayer) {
				System.out.println("finished");
				super.finished(mediaPlayer);
				mediaPlayer.pause();
				mediaPlayer.setPosition(0);
				System.gc();
			}
		});
	}
	
	private void initializeImageView() {
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        writableImage = new WritableImage((int) visualBounds.getWidth(), (int) visualBounds.getHeight());

        imageView = new ImageView(writableImage);
        playerHolder.getChildren().add(imageView);

        playerHolder.widthProperty().addListener((observable, oldValue, newValue) -> {
            fitImageViewSize(newValue.floatValue(), (float) playerHolder.getHeight());
        });

        playerHolder.heightProperty().addListener((observable, oldValue, newValue) -> {
            fitImageViewSize((float) playerHolder.getWidth(), newValue.floatValue());
        });

        videoSourceRatioProperty.addListener((observable, oldValue, newValue) -> {
            fitImageViewSize((float) playerHolder.getWidth(), (float) playerHolder.getHeight());
        });
    }
	
	private void fitImageViewSize(float width, float height) {
        Platform.runLater(() -> {
            float fitHeight = videoSourceRatioProperty.get() * width;
            if (fitHeight > height) {
                imageView.setFitHeight(height);
                double fitWidth = height / videoSourceRatioProperty.get();
                imageView.setFitWidth(fitWidth);
                imageView.setX((width - fitWidth) / 2);
                imageView.setY(0);
            }
            else {
                imageView.setFitWidth(width);
                imageView.setFitHeight(fitHeight);
                imageView.setY((height - fitHeight) / 2);
                imageView.setX(0);
            }
        });
    }
	
	private class CanvasPlayerComponent extends DirectMediaPlayerComponent {
        public CanvasPlayerComponent() {super(new CanvasBufferFormatCallback());}
        PixelWriter pixelWriter = null;
        private PixelWriter getPW() {
            if (pixelWriter == null) {pixelWriter = writableImage.getPixelWriter();}
            return pixelWriter;
        }

        @Override
        public void display(DirectMediaPlayer mediaPlayer, Memory[] nativeBuffers, BufferFormat bufferFormat) {
            if (writableImage == null) {return;}
            Platform.runLater(() -> {
                try {
                	Memory nativeBuffer = mediaPlayer.lock()[0];
                    ByteBuffer byteBuffer = nativeBuffer.getByteBuffer(0, nativeBuffer.size());
                    getPW().setPixels(0, 0, bufferFormat.getWidth(), bufferFormat.getHeight(), pixelFormat, byteBuffer, bufferFormat.getPitches()[0]);
                }
                catch (Exception ignore) {mediaPlayer.unlock();}
                finally {mediaPlayer.unlock();}
            });
        }
    }
	
	private class CanvasBufferFormatCallback implements BufferFormatCallback {
		
		Rectangle2D visualBounds;
		
        @Override
        public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
            try { visualBounds = Screen.getPrimary().getVisualBounds(); } finally {}
            Platform.runLater(() -> videoSourceRatioProperty.set((float) sourceHeight / (float) sourceWidth));
            return new RV32BufferFormat((int) visualBounds.getWidth(), (int) visualBounds.getHeight());
        }
    }
	
	public DirectMediaPlayerComponent getMediaComponent() {
		return mediaPlayerComponent;
	}
	
	
	public DirectMediaPlayer getMediaPlayer() {
		return mediaPlayerComponent.getMediaPlayer();
	}
	
	public void kill() {
		mediaPlayerComponent.getMediaPlayer().stop();
		mediaPlayerComponent.release();
	}
	
	public Pane getMediaPane() {
		return playerHolder;
	}
}
