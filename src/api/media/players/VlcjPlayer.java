package api.media.players;

import java.io.IOException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class VlcjPlayer extends AnchorPane{
	
	VlcjController vlc;	
	private Integer max_volume = 200;
	private Integer pref_volume = 100;
	private Integer jump_volume = 5;
	private Double playback_jump = 0.01;
	
	private boolean prepared;
	private ToggleButton play_pause;
	private Slider timeline_slider;
	private Button volume_controls_button;
	private VBox volume_controls;
	private Slider volume_slider;
	
	@FXML GridPane monitor;
	@FXML GridPane controls;
	
	public VlcjPlayer() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("VlcjPlayer.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {fxmlLoader.load();} catch (IOException exception) {throw new RuntimeException(exception);}
		prepared = false;
	}

	
	
	public Integer getMaxVolume() {
		return max_volume;
	}



	public void setMaxVolume(Integer max_volume) {
		this.max_volume = max_volume;
	}



	public Integer getJumpVolume() {
		return jump_volume;
	}



	public void setJumpVolume(Integer jump_volume) {
		this.jump_volume = jump_volume;
	}


	public Integer getPrefVolume() {
		return pref_volume;
	}



	public void setPrefVolume(Integer pref_volume) {
		this.pref_volume = pref_volume;
	}


	public void setPlayBackJump(Double value) {
		playback_jump = value;
	}
	
	public Double getPlayBackJump() {
		return playback_jump;
	}
	

	private void prepare() {
		try {
		//monitor.getChildren().remove(0);
		
			addEventFilter(MouseEvent.MOUSE_ENTERED, event -> showControls());
			addEventFilter(MouseEvent.MOUSE_EXITED, event -> hideControls());
			
			
			
		
			timeline_slider = new Slider(0, 1, 0);
			
			if(vlc != null) {vlc.kill();}
			
			vlc = new VlcjController(timeline_slider);
			monitor.add(new BorderPane(vlc.getMediaPane()), 0, 0);
	
			play_pause = new ToggleButton();
			play_pause.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/api/media/players/play.png"))));
			play_pause.addEventFilter(ActionEvent.ANY, event -> setPlayState(play_pause.isSelected()));
	
			controls.add(play_pause, 0, 0);
			GridPane.setFillWidth(play_pause, false);
			
			controls.add(timeline_slider, 1, 0);
			GridPane.setFillWidth(timeline_slider, true);
			
			volume_controls = new VBox();
			volume_controls.setAlignment(Pos.CENTER);
			volume_controls.setStyle("-fx-background-color: #ffffff; -fx-opacity: 0.8; -fx-border-radius: 5; -fx-background-radius: 5; -fx-padding: 5;");
			volume_controls.setVisible(false);
			AnchorPane.setRightAnchor(volume_controls, (double) 5);
			AnchorPane.setBottomAnchor(volume_controls, (double) 45);
			getChildren().add(volume_controls);
			
			Button volume_max_button = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/api/media/players/increase-volume.png"))));
			volume_max_button.addEventFilter(ActionEvent.ANY, event -> {volume_slider.setValue(max_volume);});
			volume_controls.getChildren().add(volume_max_button);
			
			volume_slider = new Slider(0, max_volume, pref_volume);
			volume_slider.setOrientation(Orientation.VERTICAL);
			volume_slider.setMinHeight(100);
			volume_slider.setPrefHeight(100);
			volume_slider.setMaxHeight(100);
			volume_slider.valueProperty().addListener(new ChangeListener<Number>() {
	
				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					vlc.getMediaPlayer().setVolume(((Double) newValue).intValue());
				}
			});
			volume_controls.getChildren().add(volume_slider);
			
			Button volume_mute_button = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/api/media/players/volume-muted.png"))));
			volume_mute_button.addEventFilter(ActionEvent.ANY, event -> {volume_slider.setValue(0);});
			volume_controls.getChildren().add(volume_mute_button);
			
			volume_controls_button = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/api/media/players/volume.png"))));
			volume_controls_button.addEventFilter(ActionEvent.ANY, event -> {volume_controls.setVisible(!volume_controls.isVisible());});
			controls.add(volume_controls_button, 2, 0);
			GridPane.setFillWidth(volume_controls_button, false);
			
			prepared = true;
		}finally {}
	}
	
	private void showControls() {
		if(prepared == true) {try {controls.setVisible(true);} finally {}};
	}
	
	private void hideControls() {
		if(prepared == true) {try {controls.setVisible(false); volume_controls.setVisible(false);} finally {}};
	}
	
	public void load(String file_path){
		if(prepared == true) {
			controls.getChildren().removeAll(controls.getChildren());
			volume_controls = null;
		}
		prepare();
		vlc.getMediaPlayer().prepareMedia(file_path);
		vlc.getMediaPlayer().start();
		vlc.getMediaPlayer().pause();
		vlc.getMediaPlayer().setPosition(0);
		vlc.getMediaPlayer().setVolume(pref_volume);
		VlcjController.gc();
	}
	
	@SuppressWarnings("static-access")
	private void setPlayState(boolean state) {
		if(state == true) {
			play_pause.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/api/media/players/pause.png"))));
			play_pause.setSelected(true);			
			vlc.addTimelineListener(timeline_slider, vlc);
			vlc.getMediaPlayer().play();
		}else {
			vlc.getMediaPlayer().pause();
			
			play_pause.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/api/media/players/play.png"))));
		}
	}
	
	public int getVolume() {
		return vlc.getMediaPlayer().getVolume();
	}
	
	public void play(Double at_position) {
		try {
			vlc.getMediaPlayer().setPosition(at_position.floatValue());
			play();
		} finally {}
	}
	
	public void play(int at_position) {
		play(Double.valueOf(at_position)/100);
	}
	
	public void play() {
		setPlayState(true);
	}
	
	public void stop() {
		try {setPlayState(false); System.gc();}finally{};
	}
	
	public void pause(Double at_position) {
		try {
			pause();
			vlc.getMediaPlayer().setPosition(at_position.floatValue());
		} finally {}
	}
	
	public void pause(int at_position) {
		pause(Double.valueOf(at_position)/100);
	}
	
	public void pause() {
		setPlayState(false);
	}
	
	private float cap(Double value) {
		if(value > 0.99) {value = 0.99;}
		else if(value < 0) {value = 0.00;}
		return value.floatValue();
	}
	
	public void forward(Double value) {
		float forward_value = cap(value) + vlc.getMediaPlayer().getPosition();
		if(forward_value > 0.99f) {
			forward_value = 0.99f;
		}
		vlc.getMediaPlayer().setPosition(forward_value);
		timeline_slider.setValue(vlc.getMediaPlayer().getPosition());
	}
	
	public void forward(int value) {
		forward(Double.valueOf(value)/100);
	}
	
	public void forward() {
		forward(playback_jump);
	}
	
	public void reverse(Double value) {
		float reverse_value = vlc.getMediaPlayer().getPosition() - cap(value);
		if(reverse_value < 0.00f) {
			reverse_value = 0.00f;
		}
		vlc.getMediaPlayer().setPosition(reverse_value);
		timeline_slider.setValue(vlc.getMediaPlayer().getPosition());
	}
	
	public void reverse(int value) {
		reverse(Double.valueOf(value)/100);
	}
	
	public void reverse() {
		reverse(playback_jump);
	}
	
	public boolean isPlaying() {
		return vlc.getMediaPlayer().isPlaying();
	}
	
	public boolean isPaused() {
		return !isPlaying();
	}
	
	public void adjustVolume(int value) {
		if(value < 0 ) {value = 0;}
		else if(value > max_volume){ value = max_volume;}
		vlc.getMediaPlayer().setVolume(value);
	}
	
	public void volumeUp(int value) {
		value = vlc.getMediaPlayer().getVolume() + value;
		adjustVolume(value);
	}
	
	public void volumeUp() {
		volumeUp(jump_volume);
	}
	
	public void volumeDown(int value) {
		value = vlc.getMediaPlayer().getVolume() - value;
		adjustVolume(value);
	}
	
	public void volumeDown() {
		volumeDown(jump_volume);
	}
	
	public void volumeMute() {
		adjustVolume(0);
	}

}