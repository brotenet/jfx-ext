package api.commons;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;

import api.commons.Environment.Session;
import api.commons.dialogs.AlertDialog;
import api.commons.dialogs.ButtonSelectionDialog;
import api.commons.dialogs.ComboSelectionDialog;
import api.commons.dialogs.ExceptionDialog;
import api.commons.dialogs.TextInputDialog;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class WindowManager {
	
	public static enum WindowLoactionPoints {UPPER_LEFT, CENTER_LEFT, BOTTOM_LEFT, UPPER_CENTER, CENTER, BOTTOM_CENTER, UPPER_RIGHT, CENTER_RIGHT, BOTTOM_RIGHT};
	public static enum WindowLocationReferences {TO_SCREEN, TO_OWNER};
	
	/**
	 * Get the parent window of the input node
	 * @param node
	 * @return
	 */
	public static Window windowOf(Node node) {
		return node.getScene().getWindow();
	}
	
	public static Window windowOf(Window window) {
		return ((Stage) window).getOwner();
	}
	
	public static void setPrimary(Window window) {
		window.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				Platform.exit();
				System.exit(0);
			}
		});
	}
	
	public static Object userDataOf(Window window) {
		return ((Stage) window).getUserData();
	}
	
	public static Object userDataOf(Node node) {
		return userDataOf(windowOf(node));
	}
	
	public static void sizeToContent(Window window) {
		((Stage) window).sizeToScene();
	}
	
	public static Parent newParent(String fxml_resource_path) throws IOException {
		return FXMLLoader.load(WindowManager.class.getResource(fxml_resource_path));
	}
	
	public static void show(Window window) {
		((Stage) window).showAndWait();
	}
	
	public static Object showAndWait(Window window) {
		((Stage) window).showAndWait();
		try {
			return ((Stage) window).getUserData();
		} catch (Exception ignore) {
			return null;
		}		
	}
	
	public Screen[] getScreens() {
		return Screen.getScreens().toArray(new Screen[Screen.getScreens().size()]);
	}
	
	public static Screen getPrimaryScree() {
		return Screen.getPrimary();
	}
	
	public static Screen[] getScreensFor(Rectangle2D dimenions) {
		return Screen.getScreensForRectangle(dimenions).toArray(new Screen[Screen.getScreensForRectangle(dimenions).size()]);
	}
	
	public static Screen[] getScreensFor(double x, double y, double width, double height) {
		return Screen.getScreensForRectangle(x, y, width, height).toArray(new Screen[Screen.getScreensForRectangle(x, y, width, height).size()]);
	}
	
	public static Screen[] getScreensFor(Window window) {
		return getScreensFor(getBounds(window));
	}
	
	public static void setLocation(Window window, double x, double y) {
		((Stage) window).setX(x);
		((Stage) window).setY(y);
	}
	
	public static void setLocation(Window window, WindowLocationReferences reference, WindowLoactionPoints location_point, double offset_x, double offset_y) {
		Rectangle2D parent_dimensions;
		Rectangle2D child_dimensions = getBounds(window);
		if(reference == WindowLocationReferences.TO_OWNER && windowOf(window) != null) {
			parent_dimensions = getBounds(getOwner(window));
		}else {
			parent_dimensions = getScreensFor(getBounds(getOwner(window)))[0].getBounds();
		}
		if(location_point == WindowLoactionPoints.UPPER_LEFT) {
			setLocation(window, parent_dimensions.getMinX() + offset_x, parent_dimensions.getMinY() + offset_y);			
		}else if(location_point == WindowLoactionPoints.CENTER_LEFT) {
			setLocation(window, parent_dimensions.getMinX() + offset_x, parent_dimensions.getMinY() + (parent_dimensions.getHeight()/2) - (child_dimensions.getHeight()/2) + offset_y);
		}else if(location_point == WindowLoactionPoints.BOTTOM_LEFT) {
			setLocation(window, parent_dimensions.getMinX() + offset_x, parent_dimensions.getMaxY() - child_dimensions.getHeight() + offset_y);
		}else if(location_point == WindowLoactionPoints.UPPER_CENTER) {
			setLocation(window, parent_dimensions.getMinX() + (parent_dimensions.getWidth()/2) - (child_dimensions.getWidth()/2) + offset_x, parent_dimensions.getMinY() + offset_y);
		}else if(location_point == WindowLoactionPoints.CENTER) {
			setLocation(window, parent_dimensions.getMinX() + (parent_dimensions.getWidth()/2) - (child_dimensions.getWidth()/2) + offset_x, parent_dimensions.getMinY() + (parent_dimensions.getHeight()/2) - (child_dimensions.getHeight()/2) + offset_y);
		}else if(location_point == WindowLoactionPoints.BOTTOM_CENTER) {
			setLocation(window, parent_dimensions.getMinX() + (parent_dimensions.getWidth()/2) - (child_dimensions.getWidth()/2) + offset_x, parent_dimensions.getMaxY() - child_dimensions.getHeight() + offset_y);
		}else if(location_point == WindowLoactionPoints.UPPER_RIGHT) {
			setLocation(window, parent_dimensions.getMaxX() - child_dimensions.getWidth() + offset_x, parent_dimensions.getMinY() + offset_y);
		}else if(location_point == WindowLoactionPoints.CENTER_RIGHT) {
			setLocation(window, parent_dimensions.getMaxX() - child_dimensions.getWidth() + offset_x, parent_dimensions.getMinY() + (parent_dimensions.getHeight()/2) - (child_dimensions.getHeight()/2) + offset_y);
		}else if(location_point == WindowLoactionPoints.BOTTOM_RIGHT) {
			setLocation(window, parent_dimensions.getMaxX() - child_dimensions.getWidth() + offset_x, parent_dimensions.getMaxY() - child_dimensions.getHeight() + offset_y);
		}
	}
	
	public static void setLocation(Window window, WindowLocationReferences reference, WindowLoactionPoints location_point) {
		setLocation(window, reference, location_point, 0, 0);
	}
	
	public static void setLocationToScreen(Window window, WindowLoactionPoints location_point, double offset_x, double offset_y) {
		setLocation(window, WindowLocationReferences.TO_SCREEN, location_point, offset_x, offset_y);
	}
	
	public static void setLocationToScreen(Window window, WindowLoactionPoints location_point) {
		setLocation(window, WindowLocationReferences.TO_SCREEN, location_point, 0, 0);
	}
	
	public static void setLocationToOwner(Window window, WindowLoactionPoints location_point, double offset_x, double offset_y) {
		setLocation(window, WindowLocationReferences.TO_OWNER, location_point, offset_x, offset_y);
	}
	
	public static void setLocationToOwner(Window window, WindowLoactionPoints location_point) {
		setLocation(window, WindowLocationReferences.TO_OWNER, location_point, 0, 0);
	}
	
	public static double getX(Window window) {
		return ((Stage) window).getX();
	}
	
	public static double getY(Window window) {
		return ((Stage) window).getY();
	}
	
	public static void setWidth(Window window, double width) {
		((Stage) window).setWidth(width);
	}
	
	public static void setMaxWidth(Window window, double width) {
		((Stage) window).setMaxWidth(width);
	}
	
	public static void setMinWidth(Window window, double width) {
		((Stage) window).setMinWidth(width);
	}
	
	public static double getWidth(Window window) {
		return ((Stage) window).getWidth();
	}
	
	public static double getMaxWidth(Window window) {
		return ((Stage) window).getMaxWidth();
	}
	
	public static double getMinWidth(Window window) {
		return ((Stage) window).getMinWidth();
	}
	
	public static void setHeight(Window window, double height) {
		((Stage) window).setHeight(height);
	}
	
	public static void setMaxHeight(Window window, double height) {
		((Stage) window).setMaxHeight(height);
	}
	
	public static void setMinHeight(Window window, double height) {
		((Stage) window).setMinHeight(height);
	}
	
	public static double getHeight(Window window) {
		return ((Stage) window).getHeight();
	}
	
	public static double getMaxHeight(Window window) {
		return ((Stage) window).getMaxHeight();
	}
	
	public static double getMinHeight(Window window) {
		return ((Stage) window).getMinHeight();
	}
	
	public static void setSize(Window window, double width, double height) {
		setWidth(window, width);
		setHeight(window, height);
	}
	
	public static void setMaxSize(Window window, double width, double height) {
		setMaxWidth(window, width);
		setMaxHeight(window, height);
	}
	
	public static void setMinSize(Window window, double width, double height) {
		setMinWidth(window, width);
		setMinHeight(window, height);
	}
	
	public static Rectangle2D getBounds(Window window) {
		return new Rectangle2D(getX(window), getY(window), getWidth(window), getHeight(window));
	}
	
	public static void setBounds(Window window, Rectangle2D bounds) {
		setSize(window, bounds.getWidth(), bounds.getHeight());
		setLocation(window, getX(window), getY(window));
	}
	
	public static void setMaximized(Window window, boolean maximized) {
		((Stage) window).setMaximized(maximized);
	}
	
	public static boolean isMaximized(Window window) {
		return ((Stage) window).isMaximized();
	}
	
	public static void setIconified(Window window, boolean iconified) {
		((Stage) window).setIconified(iconified);
	}
	
	public static boolean isIconified(Window window) {
		return ((Stage) window).isIconified();
	}
	
	public static void setAlwaysOnTop(Window window, boolean always_on_top) {
		((Stage) window).setAlwaysOnTop(always_on_top);
	}
	
	public static boolean isAlwaysOnTop(Window window) {
		return ((Stage) window).isAlwaysOnTop();
	}
	
	public static void sendToBack(Window window) {
		((Stage) window).toBack();
	}
	
	public static void sendToFront(Window window) {
		((Stage) window).toFront();
	}
	
	public static void setFullScreen(Window window, boolean full_screen) {
		((Stage) window).setFullScreen(full_screen);		
	}
	
	public static void setFocused(Window window, boolean focused) {
		window.requestFocus();
	}
	
	public static boolean isFocused(Window window) {
		return window.isFocused();
	}
	
	public static void setResizable(Window window, boolean resizable) {
		((Stage) window).setResizable(resizable);
	}
	
	public static boolean isResizable(Window window) {
		return ((Stage) window).isResizable();
	}
	
	public static void setStyle(Window window, StageStyle style) {
		((Stage) window).initStyle(style);
	}
	
	public static StageStyle getStyle(Window window) {
		return ((Stage) window).getStyle();
	}
	
	public static void setModality(Window window, Modality modality) {
		((Stage) window).initModality(modality);
	}
	
	public static Modality getModality(Window window) {
		return ((Stage) window).getModality();
	}
	
	public static void setOwner(Window window, Window owner) {
		((Stage) window).initOwner(owner);
	}
	
	public static void setTitle(Window window, String title) {
		((Stage) window).setTitle(title);
	}
	
	public static String getTitle(Window window) {
		return ((Stage) window).getTitle();
	}
	
	public static Window getOwner(Window window) {
		return ((Stage) window).getOwner();
	}
	
	public static void setImage(Window window, Image image) {
		((Stage) window).getIcons().add(image);
	}
	
	public static void setImage(Window window, String image_resource_path) {
		setImage(window, Environment.Resources.getImage(image_resource_path));
	}
	
	public static void close(Window window) {
		((Stage) window).close();
	}
	
	public static Window newWindow(String fxml_resource_path, String css_resource_path) throws IOException {
		Stage stage = new Stage();
		FXMLLoader loader = new FXMLLoader(WindowManager.class.getResource(fxml_resource_path));
		Parent root = (Parent) loader.load();
		Controller controller = (Controller) loader.getController();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		if(css_resource_path != null) {
			scene.getStylesheets().add(WindowManager.class.getResource(css_resource_path).toExternalForm());
		}
		stage.addEventFilter(WindowEvent.WINDOW_SHOWING, new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				controller.onWindowShowing(event);
			}
			
		});
		stage.addEventFilter(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				controller.onWindowShown(event);
			}
			
		});
		stage.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				controller.onWindowClose(event);
			}
			
		});
		stage.addEventFilter(WindowEvent.WINDOW_HIDDEN, new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				controller.onWindowHidden(event);
			}
			
		});
		stage.addEventFilter(WindowEvent.WINDOW_HIDING, new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				controller.onWindowHiding(event);
			}
			
		});
		return (Window) stage;
	}
	
	public static Window newWindow(String fxml_resource_path) throws IOException {
		return newWindow(fxml_resource_path, null);
	}
	
	public static class Dialogs {
		
		public static enum FilesDialogTypes {SINGLE_FILE, MULTIPLE_FILES, DIRECTORY, SAVE_FILE};
		
		/**
		 * Show an exception dialog by consuming a Throwable object
		 * @param exception
		 * @param owner
		 */
		public static void showExceptionDialog(Throwable exception, Window owner) {
			ExceptionDialog.show(exception, owner);
		}
		
		/**
		 * Show an exception dialog by consuming a Throwable object
		 * @param exception
		 * @param header_text
		 * @param owner
		 */
		public static void showExceptionDialog(Throwable exception, String header_text, Window owner) {
			ExceptionDialog.show(exception, header_text, owner);
		}
		
		public static String showComboSelectionDialog(String header_text, String content_text, String window_title, String[] items, int default_selection, Window owner) throws IOException{
			return ComboSelectionDialog.show(header_text, content_text, window_title, items, default_selection, owner);
		}
		
		public static String showComboSelectionDialog(String header_text, String content_text, String[] items, int default_selection, Window owner) throws IOException {
			return ComboSelectionDialog.show(header_text, content_text, items, default_selection, owner);
		}
		
		public static String showComboSelectionDialog(String header_text, String[] items, int default_selection, Window owner) throws IOException {
			return ComboSelectionDialog.show(header_text, items, default_selection, owner);
		}
		
		public static String showComboSelectionDialog(String[] items, int default_selection, Window owner) throws IOException {
			return ComboSelectionDialog.show(items, default_selection, owner);
		}
		
		
		public static String showButtonSelectionDialog(String header_text, String content_text, String window_title, String[] items, Window owner) throws IOException{
			return ButtonSelectionDialog.show(header_text, content_text, window_title, items, owner);
		}
		
		public static String showButtonSelectionDialog(String header_text, String content_text, String[] items, Window owner) throws IOException {
			return ButtonSelectionDialog.show(header_text, content_text, items, owner);
		}
		
		public static String showButtonSelectionDialog(String header_text, String[] items, Window owner) throws IOException {
			return ButtonSelectionDialog.show(header_text, items, owner);
		}
		
		public static String showButtonSelectionDialog(String[] items, Window owner) throws IOException {
			return ButtonSelectionDialog.show(items, owner);
		}
		
		
		public static void showAlertDialog(String header_text, String content_text, String window_title, Image image, Window owner) throws IOException{
			AlertDialog.show(header_text, content_text, window_title, image, owner);
		}
		
		public static void showAlertDialog(String header_text, String content_text, Image image, Window owner) throws IOException {
			AlertDialog.show(header_text, content_text, image, owner);
		}
		
		public static void showAlertDialog(String header_text, Image image, Window owner) throws IOException {
			AlertDialog.show(header_text, image, owner);
		}
		
		public static void showAlertDialog(String header_text, Window owner) throws IOException {
			AlertDialog.show(header_text, owner);
		}
		
		public static String showTextInputDialog(String header_text, String content_text, String window_title, String default_value, Window owner) throws IOException {
			return TextInputDialog.show(header_text, content_text, window_title, default_value, owner);
		}
		
		
		/**
		 * Show dialog for selecting files/directories for opening files or saving files
		 * @param title
		 * @param dialog_type
		 * @param extensions
		 * @param initial_directory
		 * @param default_file_name
		 * @param owner
		 * @param save_file
		 * @return
		 * @throws Exception
		 */
		public static File[] showFilesDialog(String title, FilesDialogTypes dialog_type, HashMap<String, String[]> extensions, File initial_directory, String default_file_name, Window owner, File save_file) throws Exception{
			File[] output = null;
			if(dialog_type == FilesDialogTypes.SINGLE_FILE || dialog_type == FilesDialogTypes.MULTIPLE_FILES || dialog_type == FilesDialogTypes.SAVE_FILE) {
				FileChooser dialog = new FileChooser();
				dialog.setTitle(title);
				dialog.setInitialFileName(default_file_name);
				
				if(initial_directory != null) {
					dialog.setInitialDirectory(initial_directory);
				}else {
					dialog.setInitialDirectory(new File(Session.UserHome()));
				}
				if(extensions != null) {
					for(String key : extensions.keySet()) {
						dialog.getExtensionFilters().add(new ExtensionFilter(key, extensions.get(key)));
					}
				}else {
					dialog.getExtensionFilters().add(new ExtensionFilter("All Files", "*.*"));
				}
				if(dialog_type == FilesDialogTypes.SINGLE_FILE) {
					File file = dialog.showOpenDialog(owner);
					if(file != null) {
						output = new File[] {file};
					}
				}else if(dialog_type == FilesDialogTypes.MULTIPLE_FILES){
					java.util.List<File> files = dialog.showOpenMultipleDialog(owner);
					if(files != null) {
						output = files.toArray(new File[files.size()]);
					}					
				}else if(dialog_type == FilesDialogTypes.SAVE_FILE) {
					if(save_file != null) {
						File file = dialog.showSaveDialog(owner);
						if(file != null) {
							Files.copy(Paths.get(save_file.getAbsolutePath()), Paths.get(file.getAbsolutePath()),StandardCopyOption.REPLACE_EXISTING);
						}						
					}else {
						throw new Exception("Null save_file(File) supplied");
					}
				}else {
					throw new Exception("Invalid dialog_type(FilesDialogTypes) specified");
				}
			}else if(dialog_type == FilesDialogTypes.DIRECTORY){
				DirectoryChooser dialog = new DirectoryChooser();
				dialog.setTitle(title);
				if(initial_directory != null) {
					dialog.setInitialDirectory(initial_directory);
				}else {
					dialog.setInitialDirectory(new File(Session.UserDirectory()));
				}
				output = new File[] {dialog.showDialog(owner)};
			}else {
				throw new Exception("Invalid dialog_type(FilesDialogTypes) specified");
			}
			return output;
		}
		
		/**
		 * Show dialog for selecting files/directories for opening files or saving files
		 * @param title
		 * @param dialog_type
		 * @param extensions
		 * @param initial_directory
		 * @param default_file_name
		 * @param owner
		 * @param save_file
		 * @return
		 * @throws Exception
		 */
		public static File[] showFilesDialog(String title, FilesDialogTypes dialog_type, String[] extensions, File initial_directory, String default_file_name, Window owner, File save_file) throws Exception{
			HashMap<String, String[]> extensions_map =  new HashMap<>();
			if(extensions != null) {
				for(String extension : extensions) {
					extensions_map.put(extension, new String[]{extension});
				}
			}
			return showFilesDialog(title, dialog_type, extensions_map, initial_directory, default_file_name, owner, save_file);
		}
		
		/**
		 * Show dialog for selecting files/directories for opening files or saving files
		 * @param title
		 * @param dialog_type
		 * @param extension_label
		 * @param extension
		 * @param initial_directory
		 * @param default_file_name
		 * @param owner
		 * @param save_file
		 * @return
		 * @throws Exception
		 */
		public static File[] showFilesDialog(String title, FilesDialogTypes dialog_type, String extension_label, String extension, File initial_directory, String default_file_name, Window owner, File save_file) throws Exception{
			HashMap<String, String[]> extensions_map =  new HashMap<>();
			extensions_map.put(extension_label, new String[]{extension});
			return showFilesDialog(title, dialog_type, extensions_map, initial_directory, default_file_name, owner, save_file);
		}
		
		/**
		 * Show dialog for selecting files/directories for opening files or saving files
		 * @param title
		 * @param dialog_type
		 * @param extension
		 * @param initial_directory
		 * @param default_file_name
		 * @param owner
		 * @param save_file
		 * @return
		 * @throws Exception
		 */
		public static File[] showFilesDialog(String title, FilesDialogTypes dialog_type, String extension, File initial_directory, String default_file_name, Window owner, File save_file) throws Exception{
			HashMap<String, String[]> extensions_map =  new HashMap<>();
			extensions_map.put(extension, new String[]{extension});
			return showFilesDialog(title, dialog_type, extensions_map, initial_directory, default_file_name, owner, save_file);
		}
		
	}
}
