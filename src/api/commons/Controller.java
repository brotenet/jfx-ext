package api.commons;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.stage.WindowEvent;

public class Controller {

	/**
	 * Executes while window and FXML are being initialized. The window object cannot be accessed during this operation
	 */
	@FXML public void initialize() {

	}

	/**
	 * Executes before the window is shown 
	 * @param event
	 */
	public void onWindowShowing(WindowEvent event) {
		
	}

	/**
	 * Executes after the window is loaded
	 * @param event
	 */
	public void onWindowShown(WindowEvent event) {

	}

	/**
	 * Executes when the window is hidden
	 * @param event
	 */
	public void onWindowHidden(WindowEvent event) {

	}

	/**
	 * Executes before the window is hidden
	 * @param event
	 */
	public void onWindowHiding(WindowEvent event) {

	}

	/**
	 * Executes when the window is requested to close 
	 * @param event
	 */
	public void onWindowClose(WindowEvent event) {

	}

	/**
	 * Set a data object that is accessible to both controller and window can be accessed after the window closes
	 * @param data
	 * @param root
	 */
	public static void setUserData(Object data, Node root) {
		WindowManager.windowOf(root).setUserData(data);
	}

	/**
	 * Get a data object that is accessible to both controller and window
	 * @param root
	 * @return
	 */
	public static Object getUserData(Node root) {
		return WindowManager.windowOf(root).getUserData();
	}

	/**
	 * Bind given TreeTableView to its data item class
	 * 
	 * @param control
	 * @param bind_class
	 * @param null_root
	 */
	public static void bind(TreeTableView<?> control, Class<?> bind_class, boolean null_root) {
		for (int i = 0; i < bind_class.getDeclaredFields().length; i++) {
			control.getColumns().get(i).setCellValueFactory(
					new TreeItemPropertyValueFactory<>(bind_class.getDeclaredFields()[i].getName()));
			if (null_root == true) {
				control.setRoot(new TreeItem<>(null));
				control.setShowRoot(false);
			}
		}
	}

	/**
	 * Bind given TreeTableView to its data item class
	 * 
	 * @param control
	 * @param bind_class
	 */
	public static void bind(TreeTableView<?> control, Class<?> bind_class) {
		bind(control, bind_class, true);
	}

	/**
	 * Bind given TableView to its data item class
	 * 
	 * @param control
	 * @param bind_class
	 */
	public static void bind(TableView<?> control, Class<?> bind_class) {
		for (int i = 0; i < bind_class.getDeclaredFields().length; i++) {
			control.getColumns().get(i)
					.setCellValueFactory(new PropertyValueFactory<>(bind_class.getDeclaredFields()[i].getName()));
		}
	}

	/**
	 * Bind a set of radio buttons in a toggle group and set the default selection
	 * 
	 * @param selection
	 * @param controls
	 * @return
	 */
	public static ToggleGroup bind(RadioButton selection, RadioButton... controls) {
		ToggleGroup group = new ToggleGroup();
		for (RadioButton control : controls) {
			control.setToggleGroup(group);
		}
		if (selection != null) {
			group.selectToggle(selection);
		}
		return group;
	}

	/**
	 * Bind a set of radio buttons in a toggle group and set the default selection
	 * index
	 * 
	 * @param selection
	 * @param controls
	 * @return
	 */
	public static ToggleGroup bind(int selection, RadioButton... controls) {
		ToggleGroup group = bind(null, controls);
		if (selection < 0) {
			selection = 0;
		} else if (selection >= group.getToggles().size()) {
			selection = group.getToggles().size() - 1;
		}
		group.selectToggle(group.getToggles().get(selection));
		return group;
	}

	/**
	 * Bind a set of toggle buttons in a toggle group and set the default selection
	 * 
	 * @param selection
	 * @param controls
	 * @return
	 */
	public static ToggleGroup bind(ToggleButton selection, ToggleButton... controls) {
		ToggleGroup group = new ToggleGroup();
		for (ToggleButton control : controls) {
			control.setToggleGroup(group);
		}
		if (selection != null) {
			group.selectToggle(selection);
		}
		return group;
	}

	/**
	 * Bind a set of toggle buttons in a toggle group and set the default selection
	 * index
	 * 
	 * @param selection
	 * @param controls
	 * @return
	 */
	public static ToggleGroup bind(int selection, ToggleButton... controls) {
		ToggleGroup group = bind(null, controls);
		if (selection < 0) {
			selection = 0;
		} else if (selection >= group.getToggles().size()) {
			selection = group.getToggles().size() - 1;
		}
		group.selectToggle(group.getToggles().get(selection));
		return group;
	}

	/**
	 * Set the min, max, step and selection values for a given Spinner
	 * 
	 * @param control
	 * @param min
	 * @param max
	 * @param step
	 * @param selection
	 */
	public static void bind(Spinner<Integer> control, int min, int max, int step, int selection) {
		control.setValueFactory((SpinnerValueFactory<Integer>) new SpinnerValueFactory.IntegerSpinnerValueFactory(min,
				max, selection, step));
	}

	/**
	 * Set the min, max, step and selection values for a given Spinner
	 * 
	 * @param control
	 * @param min
	 * @param max
	 * @param step
	 * @param selection
	 */
	public static void bind(Spinner<Double> control, double min, double max, double step, double selection) {
		control.setValueFactory((SpinnerValueFactory<Double>) new SpinnerValueFactory.DoubleSpinnerValueFactory(min,
				max, selection, step));
	}

	/**
	 * Set the options and selection value for a given Spinner
	 * 
	 * @param control
	 * @param items
	 * @param selection
	 */
	public static void bind(Spinner<String> control, String[] items, String selection) {
		SpinnerValueFactory<String> factory = new SpinnerValueFactory.ListSpinnerValueFactory<String>(
				FXCollections.observableArrayList(items));
		factory.setValue(selection);
		control.setValueFactory(factory);
	}
}