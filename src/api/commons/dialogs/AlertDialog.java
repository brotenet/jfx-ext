package api.commons.dialogs;

import java.io.IOException;
import java.util.HashMap;

import api.commons.Controller;
import api.commons.WindowManager;
import api.commons.WindowManager.WindowLoactionPoints;
import api.commons.WindowManager.WindowLocationReferences;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class AlertDialog extends Controller {
	
	@FXML AnchorPane root;
	@FXML Label header_text;
	@FXML Label content_text;
	@FXML ImageView header_image;
	@FXML FlowPane content;
	
	@FXML private void close() {
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onWindowShown(WindowEvent event) {
		super.onWindowShown(event);
		header_text.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("header_text")));
		if(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("content_text")).trim().length() < 1) {
			root.getChildren().remove(content);
			root.layout();
		}else {
			content_text.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("content_text")));
		}
		
		header_image.setImage((Image) ((HashMap<String, Object>) getUserData(root)).get("image"));
	}
	
	@SuppressWarnings("unchecked")
	@FXML private void accept(ActionEvent event) {
		((HashMap<String, Object>) getUserData(root)).put("output", ((Button) event.getSource()).getText());
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	@FXML private void cancel() {
		((HashMap<String, Object>) getUserData(root)).put("output", null);
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	public static void show(String header_text, String content_text, String window_title, Image image, Window owner) throws IOException{
		Window window = WindowManager.newWindow("/api/commons/dialogs/AlertDialog.fxml");
		WindowManager.setOwner(window, owner);
		HashMap<String, Object> data = new HashMap<String, Object>();

		if (header_text == null) {
			header_text = "";
		}

		if (content_text == null) {
			content_text = "";
		}

		if (window_title == null) {
			window_title = "Message";
		}
		
		if(image == null) {
			image = new Image(AlertDialog.class.getResourceAsStream("/api/commons/dialogs/warning_64.png"));
		}

		data.put("header_text", header_text);
		data.put("content_text", content_text);
		data.put("image", image);

		window.setUserData(data);
		WindowManager.setTitle(window, window_title);
		WindowManager.setImage(window, "/api/commons/dialogs/warning_16.png");
		WindowManager.setAlwaysOnTop(window, true);
		WindowManager.setResizable(window, false);
		WindowManager.setModality(window, Modality.APPLICATION_MODAL);
		if(WindowManager.getScreensFor(window).length > 0) {
			WindowManager.setLocation(window, WindowLocationReferences.TO_SCREEN, WindowLoactionPoints.CENTER);
		}
		WindowManager.sizeToContent(window);
		WindowManager.showAndWait(window);
	}
	
	public static void show(String header_text, String content_text, Image image, Window owner) throws IOException {
		show(header_text, content_text, null, image, owner);
	}
	
	public static void show(String header_text, Image image, Window owner) throws IOException {
		show(header_text, null, null, image, owner);
	}
	
	public static void show(String header_text, Window owner) throws IOException {
		show(header_text, null, null, null, owner);
	}

}
