package api.commons.dialogs;

import java.io.IOException;
import java.util.HashMap;

import api.commons.Controller;
import api.commons.WindowManager;
import api.commons.WindowManager.WindowLoactionPoints;
import api.commons.WindowManager.WindowLocationReferences;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Modality;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class ButtonSelectionDialog extends Controller {
	
	@FXML AnchorPane root;
	@FXML Label header_text;
	@FXML TilePane items_container;
	
	@FXML private void close() {
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onWindowShown(WindowEvent event) {
		super.onWindowShown(event);
		header_text.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("header_text")));
		for(String item : (String[]) ((HashMap<String, Object>) getUserData(root)).get("items")) {
			Button item_button = new Button(item);
			item_button.setOnAction(this::accept);
			item_button.setMinWidth(Button.USE_PREF_SIZE);
			item_button.setMaxWidth(Double.MAX_VALUE);
			FlowPane.setMargin(item_button, new Insets(5, 5, 5, 5));
			items_container.getChildren().add(item_button);			
		}
	}
	
	@SuppressWarnings("unchecked")
	@FXML private void accept(ActionEvent event) {
		((HashMap<String, Object>) getUserData(root)).put("output", ((Button) event.getSource()).getText());
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	@FXML private void cancel() {
		((HashMap<String, Object>) getUserData(root)).put("output", null);
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	public static String show(String header_text, String content_text, String window_title, String[] items, Window owner) throws IOException{
		Window window = WindowManager.newWindow("/api/commons/dialogs/ButtonSelectionDialog.fxml");
		WindowManager.setOwner(window, owner);
		HashMap<String, Object> data = new HashMap<String, Object>();

		if (header_text == null) {
			header_text = "Please Select one of the avalable items.";
		}

		if (content_text == null) {
			content_text = "";
		}

		if (window_title == null) {
			window_title = "Selection Input";
		}

		data.put("header_text", header_text);
		data.put("content_text", content_text);
		data.put("items", items);

		window.setUserData(data);
		WindowManager.setTitle(window, window_title);
		WindowManager.setImage(window, "/api/commons/dialogs/question_16.png");
		WindowManager.setAlwaysOnTop(window, true);
		WindowManager.setResizable(window, false);
		WindowManager.setModality(window, Modality.APPLICATION_MODAL);
		if(WindowManager.getScreensFor(window).length > 0) {
			WindowManager.setLocation(window, WindowLocationReferences.TO_SCREEN, WindowLoactionPoints.CENTER);
		}
		WindowManager.sizeToContent(window);
		WindowManager.showAndWait(window);
		return (String) ((HashMap<String, Object>) WindowManager.userDataOf(window)).get("output");
	}
	
	public static String show(String header_text, String content_text, String[] items, Window owner) throws IOException {
		return show(header_text, content_text, null, items, owner);
	}
	
	public static String show(String header_text, String[] items, Window owner) throws IOException {
		return show(header_text, null, null, items, owner);
	}
	
	public static String show(String[] items, Window owner) throws IOException {
		return show(null, null, null, items, owner);
	}

}
