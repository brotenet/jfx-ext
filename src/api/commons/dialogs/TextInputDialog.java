package api.commons.dialogs;

import java.io.IOException;
import java.util.HashMap;
import api.commons.Controller;
import api.commons.WindowManager;
import api.commons.WindowManager.WindowLoactionPoints;
import api.commons.WindowManager.WindowLocationReferences;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class TextInputDialog extends Controller {
	
	@FXML AnchorPane root;
	@FXML Label header_text, content_text;
	@FXML TextField text_field;
	
	@FXML private void close() {
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onWindowShown(WindowEvent event) {
		super.onWindowShown(event);
		header_text.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("header_text")));
		content_text.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("content_text")));
		text_field.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("default_value")));
	}
	
	@SuppressWarnings("unchecked")
	@FXML private void accept() {
		((HashMap<String, Object>) getUserData(root)).put("output", text_field.getText());
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	@FXML private void cancel() {
		((HashMap<String, Object>) getUserData(root)).put("output", null);
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	public static String show(String header_text, String content_text, String window_title, String default_value, Window owner) throws IOException{
		Window window = WindowManager.newWindow("/api/commons/dialogs/TextInputDialog.fxml");
		WindowManager.setOwner(window, owner);
		HashMap<String, Object> data = new HashMap<String, Object>();

		if (header_text == null) {
			header_text = "Please provide a text value.";
		}

		if (content_text == null) {
			content_text = "Value:";
		}
		
		if(default_value == null) {
			default_value = "";
		}

		if (window_title == null) {
			window_title = "Selection Input";
		}

		data.put("header_text", header_text);
		data.put("content_text", content_text);
		data.put("default_value", default_value);

		window.setUserData(data);
		WindowManager.setTitle(window, window_title);
		WindowManager.setImage(window, "/api/commons/dialogs/question_16.png");
		WindowManager.setAlwaysOnTop(window, true);
		WindowManager.setResizable(window, false);
		WindowManager.setModality(window, Modality.APPLICATION_MODAL);
		if(WindowManager.getScreensFor(window).length > 0) {
			WindowManager.setLocation(window, WindowLocationReferences.TO_SCREEN, WindowLoactionPoints.CENTER);
		}
		WindowManager.sizeToContent(window);
		WindowManager.showAndWait(window);
		return (String) ((HashMap<String, Object>) WindowManager.userDataOf(window)).get("output");
	}
}
