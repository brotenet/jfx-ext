package api.commons.dialogs;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

import api.commons.Controller;
import api.commons.WindowManager;
import api.commons.WindowManager.WindowLoactionPoints;
import api.commons.WindowManager.WindowLocationReferences;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class ExceptionDialog extends Controller {
	
	@FXML AnchorPane root;
	@FXML Label txtHeader;
	@FXML TextArea txtException;
	
	@FXML private void close() {
		WindowManager.close(WindowManager.windowOf(root));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onWindowShown(WindowEvent event) {
		super.onWindowShown(event);
		txtHeader.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("header_text")));
		txtException.setText(String.valueOf(((HashMap<String, Object>) getUserData(root)).get("exception_text")));
	}
	
	public static void show(Throwable exception, String header_text, String window_title, Window owner) {
		try {
			Window window = WindowManager.newWindow("/api/commons/dialogs/ExceptionDialog.fxml");
			WindowManager.setOwner(window, owner);
			HashMap<String, Object> data = new HashMap<String, Object>();
			if(header_text != null) {
				data.put("header_text", header_text);
			}else {
				data.put("header_text", exception.getMessage());
			}
			data.put("exception_text", getExceptionText(exception));
			window.setUserData(data);
			WindowManager.setTitle(window, window_title);
			WindowManager.setImage(window, "/api/commons/dialogs/error_16.png");
			WindowManager.setSize(window, 700, 600);
			WindowManager.setAlwaysOnTop(window, true);
			WindowManager.setResizable(window, false);
			WindowManager.setModality(window, Modality.APPLICATION_MODAL);
			if(WindowManager.getScreensFor(window).length > 0) {
				WindowManager.setLocation(window, WindowLocationReferences.TO_SCREEN, WindowLoactionPoints.CENTER);
			}	
			WindowManager.showAndWait(window);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void show(Throwable exception, String header_text, Window owner) {
		show(exception, header_text, "Error Exception", owner);
	}
	
	public static void show(Throwable exception, Window owner) {
		show(exception, null, "Error Exception", owner);
	}
	
	private static String getExceptionText(Throwable exception) {
		StringWriter string_writer = new StringWriter();
		PrintWriter print_writer = new PrintWriter(string_writer);
		exception.printStackTrace(print_writer);
		String output = string_writer.toString();
		return output;
	}

}
